��          �      \      �     �     �     �     �  	   �     �                         ,     9     B     R     f     y          �     �  l  �       .        G  �   W          %     .  	   6     @  
   N  0   Y  2   �  <   �     �            *   ,     W  
   m     	         
                                                                                     All all_arch all_osversion descr h3_nodata h3_title lang_en lang_fr nav_arch nav_osversion p_click2sort p_nodata tbl_pkg_caption tbl_pkg_thead_count tbl_pkg_thead_name title txt_arch txt_pkg_installed txt_version Project-Id-Version: pkgstats
PO-Revision-Date: 2020-03-06 17:45+0100
Last-Translator: Stéphane HUC <consult+trad@huc.fr.eu.org>
Language: English
Language-Team:  <consult+trad@huc.fr.eu.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: English
X-Poedit-Country: FRANCE
X-Poedit-SourceCharset: utf-8
 Tout Toutes <abbr title="Architecture">Arch.</abbr> Toutes versions Le projet 'OpenBSD :: Stats des Paquets' a pour but de créer un ensemble de statistiques sur le nombre de paquets installés sous OpenBSD, l'architecture et la version utilisée. Erreur : pas de donnée ! OpenBSD  Anglais Français Architectures OS Version (cliquez sur le nom de la colonne pour la trier) Pas de donnée. (version ou architecture inconnue) Liste des paquets installés sur OpenBSD dans cette section  Nombre Nom du paquet OpenBSD :: Stats des Paquets <abbr title="Architecture">Arch.</abbr> :  Paquets installés :  Version :  