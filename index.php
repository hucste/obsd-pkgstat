<?php
define('CMS_EXEC', true);   // defined( 'CMS_EXEC' ) or die( 'Access Denied!' );
defined('CMS_EXEC') or die('Access Denied!');

include_once('inc/constants.inc.php');
include_once('inc/functions.php');
spl_autoload_register('loadClass');

if(CMS_SESSION) {
    $session = new SessionCtlr();
    $session->sessionPosted();

    /*
    $session = new SecureSessionHandler();
    session_set_save_handler($session, true);
    $session->start();
    $session->checkSession();
    */
}

include_once('inc/db.inc.php');
include_once('inc/vars.inc.php');
include_once('inc/lang.inc.php');
//var_dump($_COOKIE);
//var_dump($_SESSION);
?>
<!DOCTYPE html>
<html dir="ltr" lang="<?php echo $language; ?>" prefix="og: http://ogp.me/ns#" scroll-behavior="smooth">
    <head>
    <link rel="stylesheet" href="/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/site.css" integrity="">

    <meta charset="utf-8">

    <title><?php echo _('title'); ?></title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-transparent">
            <div class="container">
                <div class="navbar-translate">
                    <a class="navbar-brand" href="/" rel="tooltip" title="" data-placement="bottom">
                    <img src="/img/openbsd.svg" title="OpenBSD Logo" width="64" integrity="">
                    </a>
                    <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <?php echo _('title'); ?>
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle"  id="navbarDropdownMenuLinkArch" data-toggle="dropdown"><?php echo _('nav_arch'); ?></a>
                            <span class="caret"></span>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLinkArch">
                                <a class="dropdown-item" href="index.php?arch=all" data-target="index.php"><?php echo _('all_arch'); ?></a>
                                <hr>
                            <?php
                            $result = pg_query_params($dbconn, "SELECT sys_arch,count(sys_arch) from sys GROUP BY sys_arch ORDER BY sys_arch", array());
                            if(!empty($result)) {
                                while($row = pg_fetch_row($result)) {
                                $html = '<a class="dropdown-item" href="index.php?arch='.$row[0].'" data-target="index.php">'.$row[0].' ('.$row[1].')</a>';
                                echo $html;
                                }
                            }
                            unset($result);
                            ?>

                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle"  id="navbarDropdownMenuLinkOSVersion" data-toggle="dropdown"><?php echo _('nav_osversion');; ?></a>
                            <span class="caret"></span>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLinkOSVersion">
                                <a class="dropdown-item" href="index.php?osversion=all" data-target="index.php"><?php echo _('all_osversion'); ?></a>
                                <hr>
                            <?php
                            $result = pg_query_params($dbconn, "SELECT sys_version,count(sys_version) from sys GROUP BY sys_version ORDER BY sys_version", array());
                            if(!empty($result)) {
                                while($row = pg_fetch_row($result)) {
                                $html = '<a class="dropdown-item" href="index.php?osversion='.$row[0].'" data-target="index.php">'.$row[0].' ('.$row[1].')</a>';
                                echo $html;
                                }
                            }
                            unset($result);
                            ?>

                            </div>
                        </li>

                    </ul>

                    <div class="select-style">
                        <form action="index.php" method="post">
                        <i class="fas fa-language fa-fw"></i>
                        <select class="select-style select" id="lang" onchange="location = this.value;">
                            <option id="lang-en" value="index.php?lang=en_EN" <?php if($language == "en_EN") echo "selected";?>><?php echo _('lang_en'); ?></option>
                            <option id="lang-fr" value="index.php?lang=fr_FR" <?php if($language == "fr_FR") echo "selected";?>><?php echo _('lang_fr'); ?></option>
                        </select>
                        </form>
                    </div>

                </div>
            </div>
        </nav>

    <section>
        <div class="container">
            <h1><?php echo _('title'); ?></h1>

            <?php
            include_once('inc/index.html.'.$lang.'.inc.php');
            echo '<hr>';
            include('inc/r4table-pkg.inc.php');
            ?>

        </div>
    </section>
    <footer id="bottom"></footer>
    <script src="/js/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="/js/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="/js/sorttable.js"></script>
</body>
</html>
