<?php
define('CMS_EXEC', true);   // defined( 'CMS_EXEC' ) or die( 'Access Denied!' );

include_once('inc/db.inc.php');
include_once('inc/vars.inc.php');

/*
if (isset($_POST['uuid']) &&
    isset($_POST['arch']) &&
    isset($_POST['osversion']) &&
    isset($_POST['pkglist'])) {
} else { echo "Error, a param is missing"; print_r($_POST); die(); }

$uuid =        $_POST['uuid'];
$arch =      $_POST['arch'];
$osversion = $_POST['osversion'];
$pkglist =   $_POST['pkglist'];
*/

// we check if we already known this SHA256 UUID
$result = pg_query_params($dbconn, 'SELECT sys_id from sys where sys_uuid = $1', array($uuid));
$res = pg_fetch_array($result);

// if false => new system
if(! $res) {
  echo "Hello ! You are new ! Welcome :)\n";
  $result = pg_query_params($dbconn, 'INSERT INTO sys (sys_uuid, sys_arch, sys_version) values ($1,$2,$3) RETURNING sys_id',
          array($uuid, $arch, $osversion));
  $res = pg_fetch_array($result);
  $system_id = $res[0];
} else { // system known, we update it
  echo "Updating your registered package list\n";
  $system_id = $res[0];
  pg_query_params($dbconn, 'UPDATE sys set sys_arch = $1, sys_version = $2 where sys_uuid = $3',
          array($arch, $osversion, $uuid));
}

// we remove every package for this system
pg_query_params($dbconn, 'DELETE from packages where pkg_sys = $1', array($system_id));

// we insert the packages
foreach(preg_split("/\n/", $pkglist) as $pkg) {
  if(strlen($pkg) > 1) {
    $tab[] = $pkg."\t".$system_id;
  }
}

pg_copy_from($dbconn,"packages",$tab);

?>
