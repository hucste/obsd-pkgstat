#!/bin/sh

# https://pkgstat-openbsd.perso.pw/receive.php
STATHOST="pkgstat-openbsd.perso.pw"
STATPATH="/receive.php"

OS="$(uname -s)"
if [ ! "$OS" = "OpenBSD" ]; then
	printf '%s \n' "$OS isn't supported!"
	exit 2
fi

ID="$(mount -v | sed -ne 's,.* (\(.*\)) on / .*,\1,p')"
MACHINE="$(sysctl hw.uuid; hostname)"
MACHINE_ID="$(printf '%s\n%s\n' "${ID}" "${MACHINE}" | sha256)"
ARCH="$(uname -p)"
VERSION="$(sysctl -n kern.version | awk '/^OpenBSD/ { print $2 }')"

# Package list with categories and flavors
PKG_LIST="$(pkg_info -q -P)"

DATA="uuid=${MACHINE_ID}&arch=${ARCH}&osversion=${VERSION}&pkglist=${PKG_LIST}"

unset ARCH ID MACHINE MACHINE_ID PKG_LIST VERSION

# SENDING DATAAAAAAAA !
printf '%s\r\n' \
	"POST ${STATPATH} HTTP/1.1" \
	"Host: ${STATHOST}" \
	'User-Agent: sendstats' \
	'Accept: */*' \
	"Content-length: ${#DATA}" \
	'Content-Type: application/x-www-form-urlencoded' \
	'' \
	"${DATA}" \
| nc -w 1 -c "${STATHOST}" 443
status="$?"

unset DATA STATHOST STATPATH

# Did it work ?
if [ "$status" -ne 0 ]; then
	printf '%s\n' "Could not send data"
	exit 3
fi

unset status
