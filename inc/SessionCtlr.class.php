<?php
defined('CMS_EXEC') or die('Access Denied!');

class SessionCtlr {

    //private $dev;   // devices
    private $ini;   // initialize

    public function __construct() {

        $this->cfgSession();
        $this->initSession();
        $this->runSession();

    }

    public function __destruct() {}


    public function addToCookie($name, $value) {

        $_COOKIE[$name] = $value;

    }

    public function addToSession($name, $value) {

        $_SESSION[$name] = $value;

    }

    private function initSession() {

        //if(empty($INI['secure'])) $INI['secure'] = 1;

        // initialize sessions variables
        ini_set('session.cache_expire', CMS_SESSION_LIFETIME);
        ini_set('session.cache_limiter', 'private');
        ini_set('session.cookie_domain', $this->ini['domain']);
        ini_set('session.cookie_httponly', $this->ini['httponly']);
        ini_set('session.cookie_lifetime', 0);
        ini_set('session.cookie_path', $this->ini['path']);
        ini_set('session.cookie_secure', $this->ini['secure']);

        /* deleted into 7.1.0
        if(isset($this->ini['entropy_file'])) {

            $dev = array ('/dev/urandom');

            $entropy = '';
            foreach($dev as $key => $value) {
                if(file_exists($value)) {
                    $entropy = $value;
                    break;
                }
            }
            unset($key, $value);

            if(!empty($entropy)) ini_set('session.entropy_file', $entropy);
            else ini_set('session.entropy_file', $this->ini['entropy_file']);

            if(ini_get('session.entropy_file') != '') ini_set('session.entropy_length', CMS_KEY_LENGTH);

            unset($entropy);
        }
        */

        ini_set('session.gc_divisor', 100);
        ini_set('session.gc_maxlifetime', CMS_SESSION_LIFETIME);
        ini_set('session.gc_probability', 1);
        //ini_set('session.hash_bits_per_character', 6);    # deleted into 7.1.0
        //ini_set('session.hash_function', CMS_HASH_ALGO);  # deleted into 7.1.0
        ini_set('session.lazy_write', 1);
        ini_set('session.referer_check', CMS_SRVR_NAME);
        ini_set('session.name', CMS_SESSION_NAME);
        ini_set('session.save_handler', 'files');
        ini_set('session.save_path', $this->ini['save_path']);
        ini_set('session.serialize_handler', 'php');
        ini_set('session.sid_bits_per_character', 6);   # since 7.1.0
        ini_set('session.sid_length', 256); # since 7.1.0
        ini_set('session.upload_progress.cleanup', 1);
        ini_set('session.upload_progress.enabled', 1);
        ini_set('session.url_rewriter_tags', '');
        ini_set('session.use_cookies', 1);
        ini_set('session.use_only_cookies', 1);
        ini_set('session.use_trans_sid', 0);

    }

    private function cfgSession() {

        $this->ini['domain'] = ini_get('session.cookie_domain');
        $this->ini['entropy_file'] = ini_get('session.entropy_file');
        $this->ini['httponly'] = ini_get('session.cookie_httponly');
        //$this->ini['lifetime'] = CMS_SESSION_LIFETIME;
        $this->ini['path'] = ini_get('session.cookie_path');
        $this->ini['secure'] = ini_get('session.cookie_secure');

        $this->ini['save_path'] = ini_get('session.save_path');

        if(SCHEME == 'https') $this->ini['secure'] = 1;
        //$this->ini['session_name'] = CMS_SESSION_NAME;

        if(empty($this->ini['domain'])) $this->ini['domain'] = CMS_SRVR_NAME;
        if($this->ini['httponly'] != 1) $this->ini['httponly'] = 1;

    }

    private function endingSession() {

        session_destroy();

        $session_cookie_params = session_get_cookie_params();
        setcookie(session_name(), '', time() - CMS_SESSION_LIFETIME, $session_cookie_params['path'], $session_cookie_params['domain'], $session_cookie_params['secure'], $session_cookie_params['httponly']);
        ini_set('session.cookie_lifetime', 0);

        $_SESSION = array();

    }

    #http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
    protected function preventHijacking() {

        if(!isset($_SESSION['IP']) || !isset($_SESSION['UA'])) return false;

        if($_SESSION['IP'] != CMS_WEB_IP_CLIENT) return false;

        if($_SESSION['UA'] != CMS_WEB_UA) return false;

        return true;

    }

    #http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
    protected function regenerateSession() {

        // If this session is obsolete it means there already is a new id
        if(isset($_SESSION['OBSOLETE']) || $_SESSION['OBSOLETE'] == true) return;

        // Set current session to expire in 10 seconds
        $_SESSION['OBSOLETE'] = true;
        $_SESSION['EXPIRES'] = time() + 10;

        // Create new session without destroying the old one
        session_regenerate_id(false);

        // Grab current session ID and close both sessions to allow other scripts to use them
        $newSession = session_id();
        session_write_close();

        // Set session ID to the new one, and start it back up again
        session_id($newSession);
        session_start();

        // Now we unset the obsolete and expiration values for the session we want to keep
        unset($_SESSION['OBSOLETE']);
        unset($_SESSION['EXPIRES']);

    }

    private function runSession() {

        $this->starterSession();

        $this->verifySafeSession();
        $this->verifySecretSession();
        $this->verifyTimeSession();

    }

    public function sessionPosted() {

        if($_POST) {
            $this->addToSession(CMS_SESSION_SWITCH_CSS, $_POST['set_css']);
        }
        else{
            if(empty($_SESSION[CMS_SESSION_SWITCH_CSS])) {
                $this->addToSession(CMS_SESSION_SWITCH_CSS, CMS_DEFAULT_CSS);
            }
        }

    }

    private function starterSession() {

        session_name(CMS_SESSION_NAME);

        session_set_cookie_params(CMS_SESSION_LIFETIME, $this->ini['path'], $this->ini['domain'], $this->ini['secure'], $this->ini['httponly']);

        session_start();

        #http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
        if(self::validateSession()) {

            if(!self::preventHijacking()) {

                $_SESSION = array();
                $_SESSION['IP'] = CMS_WEB_IP_CLIENT;
                $_SESSION['UA'] = CMS_WEB_UA;

                @self::regenerateSession();
            }
            elseif(rand(1, 100) <= 3){

                @self::regenerateSession();

            }

        }
        else{

            $_SESSION = array();
            session_destroy();
            session_start();

        }

    }

    #http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
    protected function validateSession() {

        if(isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES'])) return false;

        if(isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time()) return false;

        return true;
    }

    private function verifySafeSession() {

        if(!isset($_SESSION['safe'])) {
            session_regenerate_id(true);
            $this->addToSession('safe', true);

        }

    }

    private function verifySecretSession() {

        $expires = time() + CMS_SESSION_LIFETIME;
        //$secret = CypherMdlr::secureRandom();
        $secret = CypherMdlr::getSecureNonce(); var_dump($secret);
        $session_cookie_params = session_get_cookie_params();
        $session_cookie_params['expires'] = $expires;

        setcookie(CMS_SESSION_COOKIE_NAME, $secret, $expires, $session_cookie_params['path'], $session_cookie_params['domain'], $session_cookie_params['secure'], $session_cookie_params['httponly']);
        # options array is only supported since 7.3.0
        //setcookie(CMS_SESSION_COOKIE_NAME, $secret, $session_cookie_params);

        $this->addToCookie(CMS_SESSION_COOKIE_NAME, $secret);
        //$this->addToSession(CMS_SESSION_COOKIE_NAME, CypherMdlr::generateCypher($secret));
        $this->addToSession(CMS_SESSION_COOKIE_NAME, CypherMdlr::genHashSodium($secret));

        $cmp = CypherMdlr::cmpHashSodium(CypherMdlr::genHashSodium($_COOKIE[CMS_SESSION_COOKIE_NAME]), $_SESSION[CMS_SESSION_COOKIE_NAME]);

        if(!isset($_COOKIE[CMS_SESSION_COOKIE_NAME], $_SESSION[CMS_SESSION_COOKIE_NAME]) || !$cmp )
            //CypherMdlr::generateCypher($_COOKIE[CMS_SESSION_COOKIE_NAME]) != $_SESSION[CMS_SESSION_COOKIE_NAME])
        {
            // terminate session
            $this->endingSession();
        }

    }

    private function verifyTimeSession() {

        $this->addToSession('time', time());
        $session_lifetime_seconds = time() - $_SESSION['time'];
        if($session_lifetime_seconds > CMS_SESSION_LIFETIME) $this->endingSession();

    }

}
?>
