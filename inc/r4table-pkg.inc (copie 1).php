<?php
defined('CMS_EXEC') or die('Access Denied!');

/* HERE WE DISPLAY THE DATA */
$args = array();
$req  = "SELECT pkg_name,count(pkg_name) AS number from packages,sys where sys_id = pkg_sys ";

if(!empty($arch)) {
  $args[] = $arch;
  $req .= "and sys_arch = $" . count($args);
}
if(!empty($osversion)) {
  $args[] = $osversion;
  $req .= "and sys_version = $" . count($args);
}
$req .= " GROUP BY pkg_name ORDER BY pkg_name";

$result = pg_query_params($dbconn, $req, $args);
unset($args);
unset($req);
//print_r(pg_fetch_array($result));

if(pg_num_rows($result)) {
  $html = '<h3>OpenBSD :: ';
  if(!empty($arch)) $html .= 'Arch: '.$arch.' :: ';
  if(!empty($osversion))  $html .= 'Version: '.$osversion.' :: ';
  $html .= 'Packages installed:</h3>';
  $html .= '<p>(click on column name to sort)</p>';
  $html .= '<div class="table-responsive">';
  $html .= '<table class="sortable table table-hover table-sm table-striped">';
  $html .= '<caption>List of packages installed on OpenBSD</caption>';
  $html .= '<thead class="thead-dark"><tr><th scope="col">Package name</th><th scope="col">Count</th></tr></thead>';
  $html .= '<tbody>';
  while($row = pg_fetch_row($result)) {
    //    print_r($row);
    $html .= '<tr><td>'.$row[0].'</td><td class="text-right">'.$row[1].'</td></tr>';
  }
  $html .= '</tbody>';
  $html .= '<tfoot class="thead-dark"><tr><th scope="col">Package name</th><th scope="col">Count</th></tr></tfoot>';
  $html .= '</table>';
  $html .= '</div>';
} else {
  $html .= '<h3>No data, maybe you typed a wrong param (unknown version/architecture)</h3>';
}
unset($result);

if(!empty($html)) echo $html;
?>
