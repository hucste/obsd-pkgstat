<?php
defined('CMS_EXEC') or die('Acces denied!');

/* DO NOT USE WEAK ALGOS, as md5, sha1! */

class CypherMdlr {

    private $bool;
    private $cypher;
    private $function;
    public static $key;

    public function __construct() {
        $key = sodium_crypto_generichash_keygen();
    }

    public function __destruct() {}

    public static function checkHash($passwd, $dbhash) {

        $salt = substr($dbhash, 0, 64);
        $valid_hash = substr($dbhash, 64, 64);

        $test_hash = self::cypherHash($passwd, $salt);

        return hash_equals($test_hash, $valid_hash);

    }

    private static function cypherCrypt($str, $salt = null) {

        switch(CMS_CYPHER_ALGO) {
            case 'des':
            case 'md5':
                throw new Exception(__METHOD__.": Please, don't use anymore this algo: ".CMS_CYPHER_ALGO);

            case 'blowfish':
                if(CRYPT_BLOWFISH == 1) {

                    if(empty($salt)) $salt = crypt(CMS_SESSION_NAME, uniqid("", true));
                    else $salt = crypt($salt, uniqid("", true));

                    $cypher = crypt($str, $salt);
                }
            break;

            case 'sha256':
                if(CRYPT_SHA256 == 1) {

                    if(empty($salt)) $salt = crypt(CMS_SESSION_NAME, uniqid("", true));
                    else $salt = crypt($salt, uniqid("", true) );

                    $cypher = crypt($str, $salt);
                }
            break;

            case 'sha512':
                if(CRYPT_SHA512 == 1) {

                    if(empty($salt)) $salt = crypt(CMS_SESSION_NAME, uniqid("", true));
                    else $salt = crypt($salt, uniqid("", true));

                    $cypher = crypt($str, $salt);
                }
            break;

        }

        if(!empty($cypher)) return $cypher;

    }

    private static function cypherHash($str, $salt = null) {

        switch(CMS_HASH_ALGO) {
            case 'md2':
            case 'md4':
            case 'md5':
            case 'sha1':
                throw new Exception(__METHOD__.": Please, don't use anymore this algo: ".CMS_HASH_ALGO);
        }

        if(empty($salt)) $salt = hash(CMS_HASH_ALGO, CMS_SESSION_NAME);
        else $salt = hash(CMS_HASH_ALGO, $salt);

        $hash = hash(CMS_HASH_ALGO, $str.$salt);

        for($i=0; $i < CMS_KEY_LENGTH; $i++) $hash = hash(CMS_HASH_ALGO, $hash);

        if(!empty($hash)) return $hash;

    }

    public static function generateCypher($str, $salt = null) {

        if(empty($salt)) $salt = self::secureRandom();
        return self::cypherCrypt($str, $salt);;

    }

    public static function generateHash($passwd) {

        $salt = self::secureRandom();
        return $salt.self::cypherHash($passwd, $salt);

    }

    public static function secureRandom() {

        if(!is_int(CMS_KEY_LENGTH) || CMS_KEY_LENGTH <= 0)
            throw new Exception(__METHOD__.': Argument must be a positive integer.');

        if(function_exists('random_bytes'))
            $random = random_bytes(CMS_KEY_LENGTH);
        elseif(function_exists('openssl_random_pseudo_bytes'))
            $random = openssl_random_pseudo_bytes(CMS_KEY_LENGTH);
        else
            throw new Exception(__METHOD__.": OpenSSL's random_pseudo_bytes, or random_bytes function required.");

        if(!empty($random)) return bin2hex($random);
        else
            throw new Exception(__METHOD__.": Cant generate secured random value.");

    }

    public static function cmpHashSodium($buf1, $buf2) {
        return sodium_compare($buf1, $buf2);
    }

    public static function genHashSodium($secret) {
        return sodium_crypto_generichash($secret, self::$key);
    }

    public static function getSecureNonce() {
        return sodium_bin2hex(random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES));
    }


}
?>
