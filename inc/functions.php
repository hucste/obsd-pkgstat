<?php
defined('CMS_EXEC') or die('Access Denied!');

function loadClass($class) {

    $file = dirname(__FILE__).'/'.$class.'.class.php';

    if(is_file($file)) require $file;
    //else require DIR_ROOT."/vendor/autoload.php";

}

?>
