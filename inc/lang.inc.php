<?php
defined('CMS_EXEC') or die('Access Denied!');

/* get language preference */
if(!empty($_GET['lang'])) $language = $_GET['lang'];
elseif(!empty($_POST['lang'])) $language = $_POST['lang'];
elseif(!empty($_SESSION['lang'])) $language  = $_SESSION['lang'];
else $language = 'en_EN';

$language = filter_var($language, FILTER_SANITIZE_STRING);
if(CMS_SESSION) $session->addToSession('lang', $language);
//if(CMS_SESSION) $session->put('lang', $language);
else $_SESSION['lang']  = $language;

$lang = substr($language, 0, 2);

/* build language for gettext */
putenv('LANG='. $language);
setlocale(LC_ALL, $language);
$domain = 'texts';
bindtextdomain($domain, 'locale');
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);

//include('inc/lang.'.$lang.'.inc.php');
?>
