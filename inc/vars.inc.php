<?php
defined('CMS_EXEC') or die('Access Denied!');

################################################################################
### commons
# Getted|Posted variables
if(!empty($_GET['arch'])) $arch = strip_tags($_GET['arch']);
elseif(!empty($_POST['arch'])) $arch = strip_tags($_POST['arch']);
elseif(!empty($_SESSION['arch'])) $arch = strip_tags($_SESSION['arch']);
#else $arch = 0;

if(!empty($_GET['osversion'])) $osversion = strip_tags($_GET['osversion']);
elseif(!empty($_POST['osversion'])) $osversion = strip_tags($_POST['osversion']);
elseif(!empty($_SESSION['osversion'])) $osversion = strip_tags($_SESSION['osversion']);
#else $osversion = 0;

if(!empty($_POST['pkglist'])) $pkglist = strip_tags($_POST['pkglist']);

if(!empty($_POST['uuid'])) $uuid = strip_tags($_POST['uuid']);

# Sanitize variables
if(!empty($arch)) {
    $arch = filter_var($arch, FILTER_SANITIZE_STRING);
    if(CMS_SESSION) $session->addToSession('arch', $arch);
    //if(CMS_SESSION) $session->put('arch', $arch);
}
if(!empty($osversion)) {
    $osversion = filter_var($osversion, FILTER_SANITIZE_STRING);
    if(CMS_SESSION) $session->addToSession('osversion', $osversion);
    //if(CMS_SESSION) $session->put('osversion', $osversion);
}
if(!empty($pkglist)) $pkglist = filter_var($pkglist, FILTER_SANITIZE_STRING);
if(!empty($uuid)) $uuid = filter_var($uuid, FILTER_SANITIZE_STRING);

# Into the Session
/*
if(CMS_SESSION == false) {
$_SESSION['arch']  = $arch;
$_SESSION['osversion']  = $osversion;
}
*/
################################################################################
### for index
if(stristr($_SERVER['PHP_SELF'], '/index.php')) {
    if(!empty($arch)) { if(strcmp($arch, 'all') === 0) $arch = ''; }
    if(!empty($osversion)) { if(strcmp($osversion, 'all') === 0) $osversion = ''; }
}

################################################################################
### for receive.php
if(stristr($_SERVER['PHP_SELF'], '/receive.php')) {
    if(empty($arch) || empty($osversion) || empty($pkglist) || empty($uuid)) {
        echo "Error, a param is missing";
        print_r($_POST);
        die();
    }
}

?>
