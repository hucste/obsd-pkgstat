<?php
defined('CMS_EXEC') or die('Access Denied!');

/* Get DB's DATAs */
$args = array();
$req  = "SELECT pkg_name,count(pkg_name) AS number from packages,sys where sys_id = pkg_sys ";

if(!empty($arch)) {
  $args[] = $arch;
  $req .= "and sys_arch = $" . count($args);
}
if(!empty($osversion)) {
  $args[] = $osversion;
  $req .= "and sys_version = $" . count($args);
}
$req .= " GROUP BY pkg_name ORDER BY pkg_name";

$result = pg_query_params($dbconn, $req, $args);
unset($args, $req);
//print_r(pg_fetch_array($result));

/* Build Data */
if(pg_num_rows($result)) {

    $count = 0;
    $pkg = array(
        'section' => array(),
    );

    while($row = pg_fetch_row($result)) {
        //print_r($row);

        $name = explode('/', $row[0]);
        $c = count($name);

        if(! in_array($name[0], $pkg['section'])) {
            array_push($pkg['section'], $name[0]);
            $pkg[$name[0]] = array( 'name' => array(), 'nb' => array() );
        }

        /**/
        if($c == 3) {
            if(is_array($pkg[$name[0]]['name'])) array_push($pkg[$name[0]]['name'], $name[1].' > '.$name[2]);
        }
        elseif($c == 2) {
            if(is_array($pkg[$name[0]]['name'])) array_push($pkg[$name[0]]['name'], $name[1]);
        }

        if(is_array($pkg[$name[0]]['nb'])) array_push($pkg[$name[0]]['nb'], $row[1]);

        unset($name, $c);
    }

    /**/
    $html_details = '';
    foreach($pkg['section'] as $section) {
        $c1 = $c2 = 0; # counter for section
        $html_details_tr = '';

        foreach($pkg[$section]['name'] as $key => $name) {
            $html_details_tr .= '<tr><td>'.$name.'</td><td class="text-right">'.$pkg[$section]['nb'][$key].'</td></tr>';
            $count += $pkg[$section]['nb'][$key];
            $c2 += $pkg[$section]['nb'][$key];
            $c1++;
        }
        unset($key, $name);

        $html_details .= '<details>';
        $html_details .= '<summary>'.$section.': <span class="text-black-50">'.$c2.'</span></summary>';

        $html_details .= '<table class="sortable table table-hover table-sm table-striped">';
        $html_details .= '<caption>'._('tbl_pkg_caption').$section.': '.$c2.'</caption>';
        $html_details .= '<thead class="thead-dark"><tr><th scope="col">'._('tbl_pkg_thead_name').'</th><th class="text-center" scope="col">'._('tbl_pkg_thead_count').'</th></tr></thead>';
        $html_details .= '<tbody>';

        $html_details .= $html_details_tr;
        unset($html_details_tr);

        $html_details .= '</tbody>';

        if($c1 >= 20) $html_details .= '<tfoot class="thead-dark"><tr><th scope="col">'._('tbl_pkg_thead_name').'</th><th class="text-center" scope="col">'._('tbl_pkg_thead_count').'</th></tr></tfoot>';

        $html_details .= '</table>';

        $html_details .= '</details>'."\n";

        unset($c1, $c2);
    }
    unset($section, $pkg);

    /* Final HTML */
    $html = '<h3>'._('h3_title').' :: ';
    if(!empty($arch)) $html .= _('txt_arch').$arch.' :: ';
    if(!empty($osversion))  $html .= _('txt_version').$osversion.' :: ';
    $html .= _('txt_pkg_installed').'<span class="text-black-50">'.$count.'</span></h3>';
    $html .= '<p><em>'._('p_click2sort').'</em></p>';
    $html .= '<div class="table-responsive">';

    $html .= $html_details;
    unset($html_details);

    $html .= '</div>';

} else {

    $html = '<h3>'._('h3_nodata').'</h3>';
    $html .= '<p>'._('p_nodata').'</p>';

}
unset($result);

if(!empty($html)) echo $html;
unset($html);
?>
