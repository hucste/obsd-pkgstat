<?php
defined('CMS_EXEC') or die('Access Denied!');
?>
<p><strong><?php echo _('title'); ?></strong> est LE projet pour publier et recevoir des statistiques à-propos des architectures et des versions d'OpenBSD, et lister tous les paquets installés.</p>

<h2>Outils</h2>
<p><a href="/tools/send_stats.sh">Télécharger le client shell pour participer</a> : il n'envoie QUE la liste des paquets, l'architecture et la version d'OpenBSD utilisés.</p>

<h2>Données</h2>

<p>Il est possible de filtrer par architecture ou version d'OS en envoyant les paramétres <strong>arch</strong> et <strong>osversion</strong>.</p>
