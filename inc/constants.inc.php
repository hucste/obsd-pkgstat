<?php
defined('CMS_EXEC') or die('Access Denied!');

if ( empty($_SERVER['HTTPS']) ) define('SCHEME', 'http');
else define('SCHEME', 'https');

define('CMS_CYPHER_ALGO', 'sha512');    # Be carefull: need for CypherMdlr
define('CMS_HASH_ALGO', 'whirlpool');   # Be carefull: need for 'session.hash_function',
define('CMS_KEY_LENGTH', 2048);         # Be carefull: >= 4000, session not run!
//define('CMS_KEY_SECRET', CypherMdlr::secureRandom());

define('CMS_NAME', 'pkgstats');

define('CMS_SRVR_NAME', $_SERVER['SERVER_NAME']);

define('CMS_SESSION', False);
define('CMS_SESSION_LIFETIME', 60 * 60 * 24); // 1 jour
define('CMS_SESSION_NAME', CMS_NAME.strtoupper( str_replace( array(".", "_", "-"), "", CMS_SRVR_NAME ) ) );
define('CMS_SESSION_COOKIE_NAME', CMS_SESSION_NAME.'KS');

define('CMS_WEB_ACCEPT', $_SERVER['HTTP_ACCEPT']);
define('CMS_WEB_ENCODING', $_SERVER['HTTP_ACCEPT_ENCODING']);
define('CMS_WEB_IP_CLIENT', $_SERVER['REMOTE_ADDR']);
define('CMS_WEB_SRVR', $_SERVER['HTTP_HOST']);
define('CMS_WEB_URI', $_SERVER['REQUEST_URI']);
define('CMS_WEB_ROOT', CMS_WEB_SRVR.CMS_WEB_URI);
define('CMS_WEB_BASE', SCHEME."://".CMS_WEB_SRVR);
define('CMS_WEB_UA', $_SERVER['HTTP_USER_AGENT']);

?>
