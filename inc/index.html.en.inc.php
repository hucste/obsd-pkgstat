<?php
defined('CMS_EXEC') or die('Access Denied!');
?>
<p><strong><?php echo _('title'); ?></strong> is one project to publish and receive stats about OpenBSD version, architecture; and lists all packages installed. </p>

<h2>Tools</h2>
<p><a href="/tools/send_stats.sh">Download shell client to participate</a>: it only send pkglist, architecture and OpenBSD version!</p>

<h2>Datas</h2>

<p>Filtering is possible by architecture and/or os version with <strong>arch</strong> and <strong>osversion</strong> GET parameters.</p>
